# Document de Game Design pour Manju
## Principe de base
C'est un jeu de Gestion.
Le joueur contrôle une armée de Gobelin qui doivent atteindre la surface avant que la lave montante détruise tout.

## Gameplay 
Pour ce faire, les gobelins possède différentes capacités pour creuser, etc. Le joueur peut grâce à cse gobelins, construire différetns 
types de bâtiments qui feront appâratitre différenst type de gobelins.
### Types de Gobelins
- Les constructeurs 
Ces gobelins sont uniquement capable de construire des bâtiments en échange de ressources. Pour construire un bâtiment,
il faut un certain nombre de ressources et un certain nombre de gobelins. Les ressources seront définis plus loin.$*

- Les creuseurs
Ils creusent et ..c'est déjà bien

- Les récolteurs
Ils sont chargé de récolté les ressources que les creuseurs ont débloquées. Pour l'instant, lorsqu'un récolteur récupère une 
ressource, celle-ci est alors placé dans un inventaire commun. 


Les gobelins sont capable de grimper aux murs.

### Bâtiments
Il y a un bâtiments par type de Gobelins. 

### Ressources 
Ceux ci sont placé sous forme de geyser
- Le métal (ou metal, ça peut être marrant de ramasser du slayer)
- Les racines (pour le bois)

### Map
La map est généré aléatoirement, ainsi que la position de départ et la position des geysers de ressources.

### Condition de victoire et de défaite
- Victoire : Le joueur gagne lorsqu'il a atteint la surface
- Défaite : De la lave monte tout le long du jeu et détruit tout sur son passage, le joueur perd lorsqu'il a perdu tout ses bâtiments
et tous ses gobelins

## Intéraction
Pour intéragir avec son armée, le joueur dispose d'une UI où il va pouvoir définir des actions à effectuer, les gobelins se chargeront
alors de réaliser les tâches en fonction de leur capacité.
Depuis ce menu, le joueur pourra donc demander la création des bâtiments, le minage, ou la recolte. Mais il n'intéragis pas directement 
avec les gobelins.

## Déroulement d'une partie
Le joueur commence avec un nombre de ressources et un nombre de gobelins constructeurs. 
Il doit alors gérer ses gobelins du mieux possible pour construire les bâtiments adéquat pour lui fournir les bons types de gobelins. 
Le joueur doit également cherché des ressources pour constuire ces bâtiments. Au cours du jeu, la lave va monter et détuire les bâtiments,
le joueur devra alors préparer la destruction de ses bâtiments et préparer la relève afin de ne pas être à court et pouvoir
continuer la partie. 


