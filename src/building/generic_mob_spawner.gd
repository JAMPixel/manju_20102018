extends Node2D

export(float) var _spawn_rate
export(PackedScene) var _to_spawn
export(NodePath) var _destination_node

var tick = 0

signal mob_spawned

func init(spawn_rate, to_spawn, destination_node):
	_spawn_rate = spawn_rate
	_to_spawn = to_spawn
	_destination_node = destination_node

func spawn_one():
	var mob = _to_spawn.instance()
	mob.position = position
	mob.init()
	get_node(_destination_node).add_child(mob)
	return mob
	
func _process(delta):
	tick += delta
	if tick >= _spawn_rate:
		var mob = spawn_one()
		$AudioStreamPlayer.play()
		emit_signal("mob_spawned", mob)
		tick = 0

func get_size():
	return $ColorRect.get_size()