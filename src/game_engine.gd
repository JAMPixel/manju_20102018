extends Node2D

enum GAME_STATE {
	NONE,
	DIGGING,
	BUILDING,
	UPGRADING,
	CANCELLING
}

var inner_state = GAME_STATE.NONE

var astar

var mobs = []
var available_mobs = []

var mob_spawners = []

var tasks_for_mobs = []
var active_tasks = []

var tasks_map = []

onready var procedural_map = get_node("./procedural_map_2d")
onready var action_map = get_node("./action_map_2d")
onready var task_selection = get_node("./task_selection")
onready var hud = get_node("./HUD")

onready var task_manager = preload("res://src/tasks/task_manager.gd").new()

onready var is_diggable_strategy = funcref(procedural_map, "is_diggable")
onready var is_cancellable_strategy = funcref(task_manager, "is_cancellable")

onready var mob_spawner = get_node("mob_spawner")


func dig(data):
	var target = data["target"]
	var damage = data["damage"]
	var digger = data["digger"]
	var index = data["target_index"]
	var block = procedural_map.get_from_index(index)
	if block == {} or not block.has("life"):
		#To Fix!
		print("Bad block: ", target)
	else:
		block["life"] -= damage
		if block["life"] <= 0:
			
			var points = procedural_map.move_to(
				index * 32,
				procedural_map.start_point * 32 - Vector2(0, 64)
			)
			digger.move_to(points)
			procedural_map.destroy(index)
			action_map.destroy(index)
			task_manager.remove_task_by_index(index)
			
			update_avalaible_tasks()
			available_mobs.push_back(digger)

func add_mob(mob):
	mobs.push_back(mob)
	available_mobs.push_back(mob)
	
	hud.update_goblins_value(mobs.size())
	mob.connect("digging", self, "dig")

func update_avalaible_tasks():
	var tasks = task_manager.all_tasks
	tasks_for_mobs = []
	for task in tasks:
		if procedural_map.has_point(task["target"]):
			tasks_for_mobs.append(task)

func distribute_mobs(available_mobs, avalaible_tasks):
	var index = min(available_mobs.size(), avalaible_tasks.size())
	
	for i in range(0, index):
		var task = avalaible_tasks.pop_front()
		task_manager.take_task(task)
		var mob = available_mobs.pop_front()
		
		var mob_target = procedural_map.move_to(
			Vector2(mob.global_position),
			task["target"] * 32
		)
		
		mob.start_digging_to(mob_target, task["target"])


func filter_blocs_selection(zone_indices, filter_strategy):
	var final_blocs = []
	for i in range(zone_indices["begin_index"].x, zone_indices["end_index"].x):
		for j in range(zone_indices["begin_index"].y, zone_indices["end_index"].y):
			if filter_strategy.call_func(Vector2(i, j)):
				final_blocs.append(Vector2(i, j))
	return final_blocs			

func _input(event):
	if event.is_action_pressed("select_dig"):
		inner_state = GAME_STATE.DIGGING
		print("Select Dig")
	elif event.is_action_pressed("select_cancel"):
		inner_state = GAME_STATE.CANCELLING
		print("Select Cancel")

func select_blocks(zone_rect):
	var zone_indices = procedural_map.get_index_from_world_rect(zone_rect)
	if (inner_state == GAME_STATE.DIGGING):
		var final_blocs = filter_blocs_selection(zone_indices, is_diggable_strategy)
		
		task_manager.add_tasks("dig", final_blocs)
		action_map.select("dig", final_blocs)
		
		update_avalaible_tasks()
		distribute_mobs(available_mobs, tasks_for_mobs)
		
	elif (inner_state == GAME_STATE.CANCELLING):
		print("Cancelling")
		var final_blocs = filter_blocs_selection(zone_indices, is_cancellable_strategy)
		task_manager.remove_tasks(final_blocs)
		action_map.select("none", final_blocs)
		inner_state = GAME_STATE.NONE

func unselect_blocks():
	if (inner_state == GAME_STATE.SELECTING):
		inner_state = NONE
		
func set_digging_state():
	inner_state = GAME_STATE.DIGGING

func set_building_state():
	inner_state = GAME_STATE.BUILDING

func set_cancelling_state():
	inner_state = GAME_STATE.CANCELLING

func _ready():
	task_selection.connect("zone_selected", self, "select_blocks")
	task_selection.connect("zone_cleared", self, "unselect_blocks")
	
	hud.connect("digging_pressed", self, "set_digging_state")
	hud.connect("building_pressed", self, "set_building_state")
	hud.connect("cancelling_pressed", self, "set_cancelling_state")
	
	mob_spawner.connect("mob_spawned", self, "add_mob")
	
	mob_spawner.position = Vector2(22,19) * 32
	
	$AudioStreamPlayer.play()

func _process(delta):
	distribute_mobs(available_mobs, tasks_for_mobs)