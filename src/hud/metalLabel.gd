extends Label

var amount = 0

func _ready():
	update()

func update():
	text = str(amount)

func on_metal_incr():
	amount += 1

func on_metal_decr():
	amount -= 1