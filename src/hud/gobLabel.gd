extends Label

var amount = 0

func _ready():
	update()

func update():
	text = str(amount)

func on_gobelin_incr():
	amount += 1

func on_gobelin_decr():
	amount -= 1