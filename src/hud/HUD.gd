extends CanvasLayer

export (Vector2) var margin_btn
export (Vector2) var margin_res

var buttons = []
var resources = []

signal digging_pressed
signal building_pressed
signal cancelling_pressed

onready var goblins_label = $Resources/Gobelin/label
onready var metal_label = $Resources/Metal/label

onready var mining_button = $Buttons/Mining
onready var building_button = $Buttons/Building
onready var cancelling_button = $Buttons/Cancelling

func digging_emit():
	emit_signal("digging_pressed")

func building_emit():
	emit_signal("building_pressed")

func cancelling_emit():
	emit_signal("cancelling_pressed")

func update_goblins_value(value):
	goblins_label.text = str(value)	

func update_metal_value(value):
	metal_label.text = str(value)

func connect_signals():
	mining_button.connect("pressed", self, "digging_emit")
	building_button.connect("pressed", self, "building_emit")
	cancelling_button.connect("pressed", self, "cancelling_emit")

func _ready():
	buttons = $Buttons.get_children()
	resources = $Resources.get_children()
	display_btn()
	display_res()
	connect_signals()

func display_btn():
	var x = margin_btn.x
	var y = margin_btn.y
	var maxY = 0
	for i in range(buttons.size()):
		var b = buttons[i]
		if b.rect_size.y > maxY:
			maxY = b.rect_size.y
			$Buttons.rect_size.y = maxY + 2 * margin_btn.y
			$Buttons.rect_position.y = 768 - $Buttons.rect_size.y
		b.rect_position = Vector2(x, y)
		x += b.rect_size.x + margin_btn.x

func display_res():
	var x = margin_res.x
	var y = margin_res.y
	var maxY = 0
	for i in range(resources.size()):
		var r = resources[i]
		if r.get_node("icon").rect_size.y > maxY:
			maxY = r.get_node("icon").rect_size.y
			$Resources.rect_size.y = maxY + 2 * margin_res.y
		r.get_node("icon").rect_position = Vector2(x, y)
		x += r.get_node("icon").rect_size.x * r.get_node("icon").rect_scale.y + margin_res.x / 3
		r.get_node("label").rect_position = Vector2(x, y)
		x += r.get_node("label").rect_size.x + margin_res.x