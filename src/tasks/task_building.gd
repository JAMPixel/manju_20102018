extends Node

export (PackedScene) var mob_spawner
var can_build
var tilemap

var _map

func init(map, ltilemap):
	_map = map
	tilemap = ltilemap

func _input(event):
	var _building = mob_spawner.instance()
	if event is InputEventMouseButton:
		if can_build:
			_building.global_position = tilemap.world_to_map(event.position)
			_map.add_child(_building)
	elif event is InputEventMouseMotion:
		if self.can_build(_building, event.position):
			can_build = true
			Input.set_custom_mouse_cursor(load("res://icon.png"))
			print("tu peux construire ici")
		else:
			can_build = false
			Input.set_custom_mouse_cursor(load("res://icon.png"))

func can_build(building, pos):
	var ret = true
	var index = _map.get_index_from_world_rect(Rect2(pos - building.get_size()/2, building.get_size())) #dict with begin_index and end_index
	for i in range(index["begin_index"].x, index["end_index"].x):
		for j in range(index["begin_index"].y, index["end_index"].y-1):
			ret = ret and tilemap.get_cell(i,j) != -1
		ret = ret and tilemap.get_cell(i,index["end_index"].y) == -1
	return ret