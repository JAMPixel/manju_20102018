extends Node

var uuid = 0
var all_tasks = []
var available_tasks = []

const enum_task_type = {
	"dig": 1,
	"build": 2
}

enum TASK_STATE {
	AVAILABLE,
	ACTIVE
}

signal tasks_added


func is_cancellable(coordinates):
	var result = find_first(all_tasks, {"target": coordinates})
	return result["index"] != -1 and result["value"]["state"] != TASK_STATE.ACTIVE

func can_add(coordinates):
	var result = find_first(all_tasks, {"target": coordinates})
	return result["index"] == -1

func generate_uuid():
	uuid += 1
	return uuid

func check_task_type(task_type):
	return enum_task_type.has(task_type)

func create_task(task_type, target):
	var id = generate_uuid()
	
	return {
		"id": id,
		"target": target,
		"task_type": task_type,
		"type": "task",
		"state": TASK_STATE.AVAILABLE
	}

func take_task(task):
	task["state"] = TASK_STATE.ACTIVE

func task_is_available(task):
	return task["state"] == TASK_STATE.AVAILABLE

func check_task(task):
	return (
		task.has("state") or 
		task.has("id") or
		task.has("task_type") or
		task.has("type") or
		task.has("target")
		
	)

func add_tasks(task_type, targets):
	if check_task_type(task_type):
		var added = []
		for target in targets:
			if (can_add(target)):
				var task = create_task(task_type, target)
				add_task(task)
				added.append(task)
		if added.size() > 0:
			emit_signal("tasks_added", added)
	else:
		print("Invalid task type to add", task_type)

func add_task(task):
	if (check_task(task)):
		all_tasks.append(task)
	else:
		print("Cannot add the task: the argument is invalid")
		print(task)

func remove_task(task):
	if (check_task(task)):
		var to_remove = find_first(all_tasks, task)
		if to_remove["index"] != -1:
			all_tasks.remove(to_remove["index"])
	else:
		print("Cannot remove the task: the argument is invalid")

func remove_task_by_index(index):
	remove_task({"target": index})

func remove_tasks(tasks_coordinates):
	for task_coordinates in tasks_coordinates:
		remove_task({"target": task_coordinates})

func find_first(to_search, delta):
	var end = false
	var index = 0
	var result = {
		"index": -1,
		"value": null
	}
	
	while (not end):
		if index >= to_search.size():
			end = true
		else:
			var valid_item = true
			if to_search[index].has_all(delta.keys()):
				for key in delta.keys():
					if to_search[index][key] != delta[key]:
						valid_item = false
						break
				if valid_item == true:
					end = true
					result = {
						"index": index,
						"value": to_search[index]
					}
			index += 1
	return result