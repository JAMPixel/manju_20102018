extends Node2D


export(Color, RGBA) var color

var can_select = true
var must_draw = false
var begin_position
var end_position

signal zone_selected
signal zone_cleared

const bloc_size = Vector2(32,32)

func compute_rect(first_position, second_position):
	
	var begin_position = Vector2(
		min(first_position.x, second_position.x),
		min(first_position.y, second_position.y)
	)
	
	var end_position = Vector2(
		max(first_position.x, second_position.x),
		max(first_position.y, second_position.y)
	)
	
	var rect = Rect2(
		begin_position, end_position - begin_position
	)
	
	return rect

func euclidian_division(to_divide, divisor):
	var q = int(to_divide / divisor)
	var r  = to_divide - (divisor * to_divide)
	return {
		"q": q,
		"r": r
	}

func magnetic_position(pos, bloc_size):
	return Vector2(
		euclidian_division(pos.x, bloc_size.x)["q"] * bloc_size.x,
		euclidian_division(pos.y, bloc_size.y)["q"] * bloc_size.y
	)

func _input(event):
	if can_select:
		if event.is_action_pressed("selection"):
			begin_position = magnetic_position(get_global_mouse_position(), bloc_size)
			must_draw = true
		elif event.is_action_released("selection"):
			must_draw = false
			emit_signal("zone_selected", compute_rect(begin_position, end_position))
		elif event is InputEventMouseMotion:
			end_position = magnetic_position(get_global_mouse_position(), bloc_size)
	if event.is_action_pressed("clear_selection"):
		must_draw = false
		emit_signal("zone_cleared")

func _draw():
	if must_draw:
		draw_rect(
			Rect2(begin_position, end_position - begin_position),
			color
		)
		draw_rect(
			Rect2(begin_position, end_position - begin_position),
			Color(0, 0, 0, 0.7),
			false
		)

func _process(delta):
	update()