extends Node2D

onready var _tilemap = get_node("TileMap")
var last_selection

const enum_cell_actions = {
	"none": -1,
	"dig": 0
}

func select(action, blocks_id):
	if enum_cell_actions.has(action):
		last_selection = blocks_id
		for block_id in blocks_id: 
			_tilemap.set_cell(block_id.x, block_id.y, enum_cell_actions[action])

func destroy(index):
	_tilemap.set_cell(index.x, index.y, enum_cell_actions["none"])

func unselect():
	select(last_selection, "none")