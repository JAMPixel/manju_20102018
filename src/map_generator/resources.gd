var map
var x_size 
var y_size
var block = preload("block_factory.gd")
var _block = block.new()
var home 
var nb = {}
const delta = 5

func find_home():	
	var pos
	for x in range(0, x_size):
		for y in range (0, y_size):
			if map[x][y]["type"] == block.EMPTY_SPACE:
				pos = Vector2(x, y)
				return pos
	
func create_nb_by_distance():
	var coef = 2
	for i in range(0, x_size, delta):
		nb[str(i)] = pow(i,1.5)
				
		
	
func lower_nb_by_distance(key):
	for i in range (key, x_size, delta):
		nb[str(i)] -= 1

func probability_law(x):
	var maxPercentage = 30;
	var coef = -0.05
	return maxPercentage/(1+exp(coef*(x-maxPercentage)))
	
func create_repartition_map():
	var case_remaining = []
	for x in range(0, x_size):
		for y in range (0, y_size):
			case_remaining.append([x,y])
	return case_remaining
	
func random_repartition():
	create_nb_by_distance()
	home = find_home()
	var case_remaining = create_repartition_map()
	while not case_remaining.empty():
		var randCase = randi()%case_remaining.size()
		var x = case_remaining[randCase][0]
		var y = case_remaining[randCase][1] 
		case_remaining.remove(randCase)
		var distance = int(home.distance_to(Vector2(x, y)))
		var rand = randi()%100
		if rand < probability_law(distance):
			var modulo = distance - distance%delta
			if nb.has(str(modulo)) and nb[str(modulo)] > 0:
				if not map[x][y]["type"] == block.EMPTY_SPACE:
					map[x][y] = _block.generate_metal()
					lower_nb_by_distance(modulo)
		
func create_resources(map_node):
	map = map_node
	x_size = map.size()
	y_size = map[0].size()
	random_repartition()
				
	return map