extends Node
# Bloc Factory
enum block_type {
    ENVIRONMENT,
    EMPTY_SPACE,
    BUILDING,
	METAL
}

const environment = {
    'type': block_type.ENVIRONMENT
}

const empty_space = {
    'type': block_type.EMPTY_SPACE
}
const metal = {
	'type' : block_type.METAL
}
func generate_environment():
    return {
        'type': block_type.ENVIRONMENT,
		'life' : 300
    }

func generate_metal():
	return{
		'type' : block_type.METAL,
		'life' : 2000

	}
func generate_empty_space():
    return {
        'type': block_type.EMPTY_SPACE,
		'life' : 0
    }



