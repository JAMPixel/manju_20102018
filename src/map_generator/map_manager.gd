enum block_type {
    ENVIRONMENT,
    EMPTY_SPACE,
    BUILDING
}

var _size
var _map

func init(map, size):
	_size = size
	_map = map
	
func create_astar(map, size):
	var astar = AStar.new()
	var connected_points = []
	for x in range(0, size.x):
		for y in range (0, size.y ):
			var id = create_id(x, y, size)
			astar.add_point(id, Vector3(x, y, 0))
			var list = search_near_empty_point(map, size,  Vector2(x, y))
			for item in list:
				connected_points.append(item)
	var astar_result = connect_point(astar, connected_points)
	return astar_result

func connect_point(astar, connectedpoint):

	for point in connectedpoint:
		var point1 = point[0]
		var point2 = point[1]
		if not point2 in astar.get_point_connections(point1):
    		astar.connect_points(point1, point2)
	return astar
			
func search_near_empty_point(map,size, coords):
	var x = coords.x
	var y = coords.y
	var connectedpoint = []
	
	if !map[x][y]["type"] != block_type.EMPTY_SPACE: 
		var x_min = x - 1
		var x_max = x + 2
		var y_min = y - 1
		var y_max = y + 2
		
		if x == 0 :
			x_min = 0 
		elif x == size.x - 1:
			x_max = size.x 
		if y == 0 :
			y_min = 0
		elif y == size.y - 1 :
			y_max = size.y 
			
		for j in range( x_min, x_max):
			for l in range (y_min, y_max):
				if (
					(j != x or l != y) and
					map[j][l]["type"] == block_type.EMPTY_SPACE
				):
					var id = create_id(j, l, size)
					var id2 = create_id(x, y, size) 
					#print ("id :" + str(id2) + "connect with " + str(id) )
					connectedpoint.append([id, id2])
	return connectedpoint

func create_id(x, y, size):
	return y*size.x + x