extends Node

# Dependencies initalisation
const _block_factory = preload("block_factory.gd")
onready var block_factory = _block_factory.new()

const _resources = preload("resources.gd")
onready var resources = _resources.new()

export var home_size = 3

export(Vector2) var block_size = Vector2(32, 32)
export(Vector2) var dimensions = Vector2(10,10)
export(Vector2) var start_point

enum block_type {
    ENVIRONMENT,
    EMPTY_SPACE,
    BUILDING
}

onready var _tilemap = get_node('TileMap')

var data = []
var astar = AStar.new()

var empty_spaces_coords = []

func get_tilemap():
	return _tilemap

var block_id = {
	"dirt": 0,
	"metal" : 1,
	"empty_space" : 2
}

func init_map(dimensions, map):
	map = []
	for i in dimensions.x:
		map.append([])
		for j in dimensions.y:
			var block = block_factory.generate_environment()
			map[i].append(block)
	return map

func create_id(coordinates):
	return coordinates.y*dimensions.x + coordinates.x

func check_coords(dimensions, coords):
	return (
        coords.x >= 0 and coords.x < dimensions.x and
        coords.y >= 0 and coords.y < dimensions.y
    )

func create_home(dimensions, map, start_point, home_size):
	for j in range(0, home_size):
		for i in range(0, home_size):
			var to_add = [
				start_point - Vector2(i * -1, j),
				start_point - Vector2(i, j)
			]
			for block_to_add in to_add:
				if check_coords(dimensions, block_to_add):
					map[block_to_add.x][block_to_add.y] = block_factory.generate_empty_space()

	return map

func is_diggable(coordinates):
	return (
		coordinates.x >= 0 and
		coordinates.x < dimensions.x and
		coordinates.y >= 0 and
		coordinates.y < dimensions.y and
		(data[coordinates.x][coordinates.y].type == block_id["dirt"])
	)

func set_block(map, index):
	match(map[index.x][index.y]["type"]):
		0:
			_tilemap.set_cell(index.x, index.y, block_id["dirt"])
		3:
			_tilemap.set_cell(index.x, index.y, block_id["metal"])
		1:
			_tilemap.set_cell(index.x, index.y, block_id["empty_space"])
	
func instance_blocks(dimensions, map):
	for i in dimensions.x:
		for j in dimensions.y:
			set_block(map, Vector2(i, j))


func generate(dimensions):
	var map
	map = init_map(dimensions, map)
	map = create_home(dimensions, map, start_point, home_size)
	map = resources.create_resources(map)
	instance_blocks(dimensions, map)
	return map

func get_index_from_pos(world_position):
	return Vector2(
		int(world_position.x / block_size.x), 
		int(world_position.y / block_size.y)
	)

func get_index_from_world_rect(world_position):
	var begin_index = Vector2(
		int(world_position.position.x / block_size.x), 
		int(world_position.position.y / block_size.y)
	)
	var end_index = Vector2(
		int((world_position.position.x + world_position.size.x) / block_size.x),
		int((world_position.position.y + world_position.size.y) / block_size.y)	
	)
	
	return {
		"begin_index": begin_index,
		"end_index": end_index
	}



func old_init_astar(map):
	for i in range(1, dimensions.x - 1):
		for j in range(1, dimensions.y - 1):
			if map[i][j]["type"] == block_type.EMPTY_SPACE:
				astar.add_point(create_id(Vector2(i, j)), Vector3(i, j, 0))
			if i > 0:
				if map[i - 1][j]["type"] == block_type.EMPTY_SPACE:
					astar.connect_points(create_id(Vector2(i, j)), create_id(Vector2(i-1, j)))
			if j > 0:
				if map[i][j - 1]["type"] == block_type.EMPTY_SPACE:
					astar.connect_points(create_id(Vector2(i, j)), create_id(Vector2(i, j - 1)))

func astar_add_point(index):
	var map = data
	var i = index.x
	var j = index.y
	var id = create_id(Vector2(i, j))
	
	if (not astar.has_point(id) and (
		map[i - 1][j]["type"] 	== block_type.EMPTY_SPACE or
		map[i + 1][j]["type"] 	== block_type.EMPTY_SPACE or
		map[i][j - 1]["type"] 	== block_type.EMPTY_SPACE or
		map[i][j + 1]["type"] 	== block_type.EMPTY_SPACE or
		map[i][j]["type"] 		== block_type.EMPTY_SPACE
	)):
		astar.add_point(id, Vector3(i, j, 0))
	
	if astar.has_point(id):
		
		var to_connect = [
			Vector2(i-1, j),
			Vector2(i+1, j),
			Vector2(i, j - 1),
			Vector2(i, j + 1)
		]
		
		for to_connect_item in to_connect:
			var to_co_id = create_id(to_connect_item)
			if astar.has_point(to_co_id ) and not id in astar.get_point_connections(to_co_id ):
				astar.connect_points(id, to_co_id )
		
	return astar

func init_astar(map):
	data = map
	for i in range(1, dimensions.x - 1):
		for j in range(1, dimensions.y - 1):
			astar = astar_add_point(Vector2(i, j))
	return astar

func move_to(from, to):	
	var magnetic_from = Vector2(
		int(from.x / block_size.x),
		int(from.y / block_size.y)
	) 
	var magnetic_to = Vector2(int(to.x / block_size.x),int(to.y / block_size.y))
	
	var points_2d = []
	var points = astar.get_point_path(create_id(magnetic_from), create_id(magnetic_to))

	for point in points:
		var point_2d = Vector2(point.x * block_size.x + 16, point.y * block_size.y + 16)
		points_2d.append(point_2d)
	return points_2d
	
func get_from_position(coords):
	var index = get_index_from_pos(coords)
	var result = -1
	if check_coords(block_size, index):
		result = data[index.x][index.y]
	return result

func get_from_index(index):
	return data[index.x][index.y]

func destroy(index):
	data[index.x][index.y] = block_factory.generate_empty_space()
	set_block(data, index)
	astar_add_point(index + Vector2(-1, 0))
	astar_add_point(index + Vector2(+1, 0))
	astar_add_point(index + Vector2(0, -1))
	astar_add_point(index + Vector2(0, +1))

func _ready():
	var map = generate(dimensions)
	init_astar(map)
	data = generate(dimensions)

func has_point(point):
	return astar.has_point(create_id(point))
	
func hit(x, y):
	data[x][y] = block_factory.generate_empty_space()
	_tilemap.set_cell(x, y, block_id["dirt"])