extends Path2D

export var SPEED = 200
export var damage = 150

enum MOB_STATE {
	DO_NOTHING,
	MOVING,
	DIGGING,
	GO_DIGGING
}
var inner_state
var index_to_dig

onready var path_follow = get_node("PathFollow2D")

signal digging

func is_available():
	return inner_state == MOB_STATE.DO_NOTHING

func set_points(points):
	$PathFollow2D/AnimatedSprite.play()
	curve.clear_points()
	for point in points:
		curve.add_point(point - position)
	path_follow.set_offset(0)
	path_follow.unit_offset = 0

func move_to(points):
	set_points(points)
	inner_state = MOB_STATE.MOVING

func waiting():
	set_points([position])
	inner_state == MOB_STATE.DO_NOTHING

func init():
	inner_state = MOB_STATE.DO_NOTHING
	curve = Curve2D.new()
	curve.clear_points()
	curve.add_point(Vector2(0,0))
	
func _ready():
	inner_state = MOB_STATE.DO_NOTHING
	init()
	path_follow.loop = false
	
func move(delta):
	path_follow.set_offset(path_follow.get_offset() + SPEED * delta)
	if ((1.0 - path_follow.unit_offset) <= 0.1):
		if (inner_state == MOB_STATE.GO_DIGGING):
			inner_state = MOB_STATE.DIGGING
		elif (inner_state == MOB_STATE.MOVING):
			waiting()
			

func start_digging_to(points, index):
	set_points(points)
	index_to_dig = index
	inner_state = MOB_STATE.GO_DIGGING

func _process(delta):
	if ((inner_state == MOB_STATE.MOVING) or inner_state == MOB_STATE.GO_DIGGING):
		move(delta)
	elif(inner_state == MOB_STATE.DIGGING):
		emit_signal("digging", {
			"target": position,
			"target_index": index_to_dig,
			"damage": delta * damage,
			"digger": self
		})