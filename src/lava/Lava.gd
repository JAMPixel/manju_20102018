extends Node

signal game_over

func _ready():
	pass

func _process(delta):
	$TextureProgress.value += delta*5
	if $TextureProgress.value >= 100:
		emit_signal("game_over")