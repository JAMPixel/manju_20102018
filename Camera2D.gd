extends Camera2D
export (float) var deltaZoom = 0.02
export (float) var deltaScroll = 10
export (float) var delta_before_scroll = 0.05
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass
func _process(delta):
	scroll(get_viewport().get_mouse_position())
	
func _input(ev):
	
	var deltaV2 = Vector2(deltaZoom, deltaZoom)
	if ev is InputEventMouseButton :
		if ev.is_pressed():
			zoom(deltaV2, ev.button_index)
        # zoom in
			
				
				
func scroll (mouse_position):
	
	var max_scroll = 1 - delta_before_scroll
	var min_scroll = delta_before_scroll
	var scroll = deltaScroll*(1-self.zoom.x)
	var xPercentage = mouse_position.x/get_viewport_rect().size.x
	var yPercentage = mouse_position.y/get_viewport_rect().size.y
	if xPercentage > max_scroll:
		self.global_position.x += deltaScroll
	if xPercentage < min_scroll:
		self.global_position.x -= deltaScroll
	if yPercentage > max_scroll:
		self.global_position.y += deltaScroll
	if yPercentage < min_scroll:
		self.global_position.y -= deltaScroll
			

	
	
        
func zoom(delta, button_index):
	
	if button_index == BUTTON_WHEEL_UP:
		self.zoom -= delta
		if self.zoom.x < 0 or self.zoom.y < 0:
			self.zoom = delta
	if button_index == BUTTON_WHEEL_DOWN:
		self.zoom += delta
		
