# Manju

L'objectif est d'emmener les gobelins à la surface le plus rapidement possible. Au moins avant qu'ils meurent tous dans la lave.
Pour ce faire, les gobelins peuvent creuser des blocs : cliquez sur le bouton *DIG* puis selectionnez les bloc à creuser en dessinant un rectangle de sélection.
Si vous avez sélectionné des blocs que vous ne vouliez pas creuser. Cliquez sur le bouton *CANCEL* puis sélectionnez de la même manière que précédemment les blocs que vous ne voulez plus creuser.
Vous pouvez créer des bâtiments qui fabriquent des gobelins en cliquant sur le bouton *BUILD*. **Ca marche pas**