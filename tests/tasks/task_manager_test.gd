extends Node

const _task_manager = preload("res://src/tasks/task_manager.gd")

func generate_uuid_test():
	var task_manager = _task_manager.new()
	
	var uuid_1 = task_manager.generate_uuid()
	assert(uuid_1 == 1)
	
	var uuid_2 = task_manager.generate_uuid()
	assert(uuid_2 == 2)

func create_tasks_test():
	var task_manager = _task_manager.new()
	var task_target = Vector2(0, 0)
	var created_task = task_manager.create_task(task_manager.TASK_TYPE.DIG_ACTION, task_target)
	assert(created_task["id"] == 1)
	assert(created_task["type"] == "task")
	assert(created_task["task_type"] == task_manager.TASK_TYPE.DIG_ACTION)
	assert(created_task["target"] == task_target)
	
func add_task_test():
	var task_manager = _task_manager.new()
	var task_target = Vector2(0, 0)
	var created_task = task_manager.create_task(task_manager.TASK_TYPE.DIG_ACTION, task_target)
	task_manager.add_task(created_task)
	
	assert(task_manager.all_tasks.size() == 1)
	
func remove_task_test():
	# Given a task Manager
	var task_manager = _task_manager.new()
	# And A Specific Task
	var task_target = Vector2(0, 0)
	var created_task = task_manager.create_task(task_manager.TASK_TYPE.DIG_ACTION, task_target)
	task_manager.add_task(created_task)
	assert(task_manager.all_tasks.size() == 1)
	
	# When I want to Remove it
	task_manager.remove_task(created_task)
	
	# Then, the All_tasks collection has none elements
	assert(task_manager.all_tasks.size() == 0)


func run_test(to_call):
	print(to_call)
	funcref(self, to_call).call_func()
	print(to_call + " -- Ok")
	print("---------------------------")

func _ready():
	print("---------------------------")
	print("---------------------------")
	print("# Task_manager_test")	
	print("---------------------------")
	run_test("generate_uuid_test")
	run_test("create_tasks_test")
	run_test("add_task_test")
	run_test("remove_task_test")
	pass