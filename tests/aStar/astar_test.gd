extends Node

const _astar = preload("res://src/map_generator/map_manager.gd")
const _block = preload("res://src/map_generator/block_factory.gd")
var block = _block.new()
func pathTest():
	var astar = _astar.new()
	astar.map = [
	[block.generate_empty_space(),
	block.generate_empty_space(),
	block.generate_empty_space()],
	 [block.generate_empty_space(),
	block.generate_empty_space(),
	block.generate_empty_space()], 
	[block.generate_environment(),
	block.generate_empty_space(),
	block.generate_empty_space()] 
	]
	astar.create_astar()
	var connect05 = astar.astar.get_point_path(0, 5)
	assert(connect05.size() != 0)
	var connect02 = astar.astar.get_point_path(0, 2)
	assert(connect02.size() == 0)
	
func search_test():
	var astar = _astar.new()
	astar.map = [[block.generate_empty_space(),
	block.generate_empty_space(),
	block.generate_empty_space()],
	 [block.generate_empty_space(),
	block.generate_empty_space(),
	block.generate_empty_space()], 
	[block.generate_empty_space(),
	block.generate_empty_space(),
	block.generate_empty_space()] ]
	astar.init()
	astar.search_near_empty_point(0,0)
	var a = astar.connectedpoint.size() 
	assert (astar.connectedpoint.size() == 3)
	
func run_test(to_call):
	print(to_call)
	funcref(self, to_call).call_func()
	print(to_call + " -- Ok")
	print("---------------------------")

func _ready():
	print("---------------------------")
	print("---------------------------")
	print("# Task_manager_test")	
	print("---------------------------")
	run_test("search_test")
	run_test("pathTest")
	
	pass