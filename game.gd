extends Node2D

onready var task_building = get_node("task_building")
onready var procedural_map = get_node("procedural_map_2d")

func _ready():
	var map = procedural_map
	task_building.init(map, procedural_map.get_tilemap())
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass